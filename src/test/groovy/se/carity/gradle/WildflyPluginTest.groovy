package se.carity.gradle

import org.junit.Test
import org.gradle.testfixtures.ProjectBuilder
import org.gradle.api.Project
import static org.junit.Assert.*

class WildflyPluginTest {

    @Test
    public void test_plugin_add_task_to_project() {
        Project project = ProjectBuilder.builder().build()
        project.getPlugins().apply 'se.carity.gradle.wildfly.plugin'
        assertTrue(project.tasks.wildflyDeploy instanceof WildflyDeployTask)
    }
}