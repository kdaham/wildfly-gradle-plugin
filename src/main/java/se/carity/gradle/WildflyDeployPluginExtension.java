package se.carity.gradle;

/**
 * Created by khaled on 30/08/16.
 */
public class WildflyDeployPluginExtension {
    private String hostname = "localhost";
    private String port = "9990";

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private String username = "";
    private String password = "";
}
