package se.carity.gradle;

import org.gradle.api.Plugin;
import org.gradle.api.Project;

/**
 * Created by khaled on 30/08/16.
 */
public class WildflyDeployPlugin implements Plugin<Project> {

    @Override
    public void apply(Project project) {
        project.getExtensions().create("wildfly", WildflyDeployPluginExtension.class);
        project.getTasks().create("deploy", WildflyDeployTask.class);
    }
}
