package se.carity.gradle;

import org.jboss.as.cli.CliInitializationException;
import org.jboss.as.cli.CommandContext;
import org.jboss.as.cli.CommandContextFactory;
import org.jboss.as.cli.CommandLineException;
import org.jboss.as.cli.impl.CommandContextConfiguration;

import java.net.URI;
import java.net.URISyntaxException;

public class WildflyDeployer {
    private CommandContext ctx;

    public WildflyDeployPluginExtension getExtension() {
        return extension;
    }

    public void setExtension(WildflyDeployPluginExtension extension) {
        this.extension = extension;
    }

    private WildflyDeployPluginExtension extension;

    public WildflyDeployer(WildflyDeployPluginExtension extension) {
        this.setExtension(extension);
        initCommandContext();
    }

    public CommandContext getCtx() {
        return ctx;
    }

    public void setCtx(CommandContext ctx) {
        this.ctx = ctx;
    }

    private void initCommandContext() {
        try {
            if(extension.getUsername().isEmpty()) {
                ctx = CommandContextFactory.getInstance().newCommandContext();
            } else {
                ctx = CommandContextFactory.getInstance().newCommandContext(
                        new CommandContextConfiguration.Builder().setController(
                                new URI("http-remoting", null, extension.getHostname(), Integer.parseInt(extension.getPort()), null, null, null).toString())
                                .setUsername(extension.getUsername())
                                .setPassword(extension.getPassword().toCharArray())
                                .setClientBindAddress(null)
                                .build());
            }
        } catch(CliInitializationException e) {
            throw new IllegalStateException("Failed to initialize CLI context", e);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

    public boolean deploy(String archive) {
        if(ctx == null) {
            initCommandContext();
        }
        try {
            ctx.connectController();
            ctx.handle("deploy \"" + archive + "\" --force");
        } catch (CommandLineException e) {
            e.printStackTrace();
            return false;
        } finally {
            ctx.terminateSession();
        }
        return true;
    }
}
