package se.carity.gradle;

import org.gradle.api.DefaultTask;
import org.gradle.api.plugins.WarPlugin;
import org.gradle.api.tasks.TaskAction;
import org.gradle.api.tasks.bundling.War;

public class WildflyDeployTask extends DefaultTask {

    WildflyDeployPluginExtension extension;

    @TaskAction
    public void deploy() {
        extension = getProject().getExtensions().findByType(WildflyDeployPluginExtension.class);
        if (extension == null) {
            extension = new WildflyDeployPluginExtension();
        }
        War warTask = (War) getProject().getTasks().getByName(WarPlugin.WAR_TASK_NAME);
        String archive = warTask.getArchivePath().getAbsolutePath();
        WildflyDeployer deployer = new WildflyDeployer(extension);
        try {
            System.out.println("===   Deploying " + archive + "    ===");
            deployer.deploy(archive);
        } catch (Exception ex) {
            System.err.println("###   Exception occurred during deploying to Wildfly ###");
            System.err.println(ex.getMessage());
        }
    }
}
